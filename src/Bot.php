<?php

require '../vendor/autoload.php';

include("../Request.php");
include("../Response.php");
include("_Telegram/TelegramBot.php");
include("UserModel.php");
include("TransaksiModel.php");
include("MateriModel.php");
include("../Connection.php");
include("LogChatModel.php");

define('SCOPES', implode(' ', array(Google_Service_Drive::DRIVE)));
putenv('GOOGLE_APPLICATION_CREDENTIALS=/home/develo16/public_html/MohammadChicoHernando/API_Telegram/storage/credential/Botutteracademy.json');

$app = new \Slim\App();

$app->post("/telegram", Bot::class . ":telegram");

class Bot
{
    public function __construct()
    {
        $this->request = new Request();
        $this->response = new Response();
        $this->telegramBot = new \Pos\Telegram\telegramBot();
        $this->usermodel = new UserModel();
        $this->transaksimodel = new TransaksiModel();
        $this->materimodel = new MateriModel();
        $this->logchatmodel = new LogChatModel();
    }

    public function telegram($request, $response, $args)
    {
        $this->processTelegram($request);
    }

    public function processTelegram($request)
    {
        $body = $request->getBody();
        $this->addLog($body);
//        $this->send_message(677129765,$body);
        $message = json_decode($body, true);
        $this->process_message($message, $request);
    }

    function process_message($message, $request)
    {
        $callback_query = $message["callback_query"];
        if (isset($callback_query)) {
            $data = $callback_query["data"];
            $message = $callback_query["message"];
            $messageId = $callback_query["message"]["message_id"];
            $chatId = $message["chat"]["id"];
            $this->deleteMessage($chatId, $messageId);
            $this->processMessageCommand($chatId, $data);
        } else {
            $updateid = $message["update_id"];
            $message_data = $message["message"];
            $chatid = $message_data["chat"]["id"];
            $message_id = $message_data["message_id"];
            $response = "";
            if ($message_data["entities"][0]["type"] == "email") {
                $this->updateUser($chatid, $message_data["text"]);
            } elseif (isset($message_data["text"])) {
                $text = $message_data["text"];
                $cekLog = $this->logchatmodel->getLog($chatid);
                if ($cekLog->text == "Masukkan Judul Materi Java yang ingin ditambahkan" || $cekLog->text == "Masukkan Judul Materi Kotlin yang ingin ditambahkan" || $cekLog->text == "Masukkan Judul Materi Varsion Control yang ingin ditambahkan") {
                    $this->send_message($chatid, "judul materi yang akan ditambahkan : " . $text);
                    $jenis = explode(" ", $cekLog->text);
                    $this->materimodel->createJudul([
                        ":user_id" => $chatid,
                        ":jenis_materi" => $jenis[3],
                        ":judul_materi" => $text
                    ]);
                } else {
                    $this->processMessageCommand($chatid, $text, $message_data);
                }
            }
        }
        return $updateid;
    }

    function processMessageCommand($chatid, $text, $data = null, $messageId = null)
    {
        $markup = null;
        $message = null;

        if (strpos($text, ":konfirmasi") !== false) {
            $splitText = explode(" ", $text);
            $this->transaksimodel->confirmAdminTransaksi($splitText[2]);
            $this->processShareFile($splitText[3]);
            $this->send_message($chatid, "Anda mengkonfirmasi pembayaran " . $splitText[2]);
        } else {
            switch ($text) {
                case "/kelas":
                    $keyboard = array(
                        'inline_keyboard' => [
                            [
                                ['text' => 'Java', 'callback_data' => ':java'],
                                ['text' => 'kotlin', 'callback_data' => ':kotlin']
                            ],
                            [
                                ['text' => 'version control', 'callback_data' => ':version_control']
                            ]
                        ],
                        'resize_keyboard' => true,
                        'one_time_keyboard' => true
                    );
                    $message = "Silakan pilih kelas yang ingin dipelajari :";
                    $markup = json_encode($keyboard, true);
                    break;
                case "/menu":
                    if ($chatid == "155152745" || $chatid == "677129765") {
                        $keyboard = array(
                            'inline_keyboard' => [
                                [['text' => 'Konfirmasi Pembelian', 'callback_data' => '/admin_konfirmasi']],
                                [['text' => 'Tambah Materi', 'callback_data' => '/tambah_materi']]
                            ],
                            'resize_keyboard' => true,
                            'one_time_keyboard' => true
                        );
                        $message = "Menu untuk Admin :";
                        $markup = json_encode($keyboard, true);
                    }
                    break;
                case "/start":
                    $this->send_message($chatid, "HAI saya adalah bot untuk mempelajari bahasa pemrogaman Java, Kotlin dan Version Control");
                    $this->insertUser($data);
                    break;
                case "/java":
                    $this->send_message($chatid, "Belajar materi Java mulai dari 0");
                    break;
                case "/kotlin":
                    $this->send_message($chatid, "Belajar materi Kotlin mulai dari 0");
                    break;
                case "/version_control":
                    $this->send_message($chatid, "Belajar version_control mulai dari 0");
                    break;
                case "/konfirmasi":
//                $this->send_message($chatid,"Perintah untuk konfirmasi pembelian");
                    $this->konfirmasiTransaksi($chatid, $data);
                    break;
                case "/admin_konfirmasi":
                    if ($chatid == "155152745" || $chatid == "677129765") {
                        $transaksi = $this->transaksimodel->getUnconfirmedTransaksi();
                        $pesan = "User yang sudah memesan tapi belum konfirmasi admin :\n";

                        $keyboard = array(
                            'inline_keyboard' => [],
                            'resize_keyboard' => true,
                            'one_time_keyboard' => true
                        );

                        for ($i = 0; $i < sizeof($transaksi); $i++) {
                            $pesan .= $transaksi[$i]["username"] . " -- " . $transaksi[$i]["fullname"] . " -- " . $transaksi[$i]["barang"] . " -- " . $transaksi[$i]["total"] . "\n";
                            array_push($keyboard["inline_keyboard"], [['text' => $transaksi[$i]["fullname"], 'callback_data' => 'admin :konfirmasi ' . $transaksi[$i]["username"] . ' ' . $transaksi[$i]["user_id"]]]);
                        }
                        $pesan .= "\nSilakan pilih nama user yang sudah membayar :";
                        $message = $pesan;
                        $markup = json_encode($keyboard, true);
                    }
                    break;
                case "/tambah_materi":
                    if ($chatid == "155152745" || $chatid == "677129765") {
                        $keyboard = array(
                            'inline_keyboard' => [
                                [['text' => 'JAVA', 'callback_data' => ':tambah_java']],
                                [['text' => 'KOTLIN', 'callback_data' => ':tambah_kotlin']],
                                [['text' => 'VERSION CONTROL', 'callback_data' => ':tambah_vc']],
                                [['text' => 'Google Docs', 'callback_data' => ':tambah_gdrive']]
                            ],
                            'resize_keyboard' => true,
                            'one_time_keyboard' => true
                        );
                        $message = "Pilih materi yang akan ditambahkan :";
                        $markup = json_encode($keyboard, true);
                    }
                    break;
                case ":tambah_java":
                    $this->send_message($chatid, "Masukkan Judul Materi Java yang ingin ditambahkan");
                    break;
                case ":tambah_kotlin":
                    $this->send_message($chatid, "Masukkan Judul Materi Kotlin yang ingin ditambahkan");
                    break;
                case ":tambah_vc":
                    $this->send_message($chatid, "Masukkan Judul Materi Varsion Control yang ingin ditambahkan");
                    break;
                case ":tambah_gdrive":
                    $this->send_message($chatid, "Masukkan dengan format\nmateri google drive [jenis materi(JAVA/KOTLIN/VERSION_CONTROL)] [judul] [id file] \nContoh : \nmateri google drive KOTLIN kotlin untuk pemula 10k308183k3hfyhef8\n\n- id file bisa didapatkan dari link share (https://drive.google.com/file/d/[file id]/)\n- pastikan file berada di dalam folder yang email bot utter academy mempunyai permission edit\n- email bot utter academy = botutter-academy@bot-utter-academy.iam.gserviceaccount.com");
                    break;
                case ":java":
                    $this->createTransaksi("JAVA", $chatid);
//                $this->send_message($chatid,"Mempelajari Materi Java");
                    break;
                case ":kotlin":
                    $this->createTransaksi("KOTLIN", $chatid);
//                $this->send_message($chatid,"Mempelajari Materi Kotlin");
                    break;
                case ":version_control":
                    $this->createTransaksi("Git", $chatid);
//                $this->send_message($chatid,"Mempelajari Materi Version control");
                    break;
                case "/share":
//                    $user_email = $this->usermodel->getUserById($chatid);
//                    if (isset($user_email->email) != true) {
//                        $this->send_message($chatid, "Saya tidak tahu emailmu, biar bisa memakai fitur ini tolong ketikkan Emailmu");
//                    } else
//                        $this->shareFile($chatid);
                    $this->processPermission($chatid);
//                    $this->processShareFile($chatid);
                    break;
                case "/tes":
                    $this->tes($chatid);
                    break;
                case "/tesP":
                    $this->tesPermission($chatid,1);
                    break;
                default:
                    $this->processMessageTeksComplex($chatid, $text, $messageId);
                    return;
            }
            if ($messageId == null) {
                $this->send_message($chatid, $message, $markup);
            } else {
                $this->editMessageReplyMarkup($chatid, $messageId, $message, $markup);
            }
        }
    }

    function konfirmasiTransaksi($chatId, $data)
    {
        $transaksi = $this->transaksimodel->lihatTransaksi($chatId);
        for ($i = 0; $i < sizeof($transaksi); $i++) {
            $status[] = $transaksi[$i]["status"];
        }
        if (in_array(0, $status) != true) {
            $this->send_message($chatId, "Anda belum mempunyai pembelian untuk dikonfirmasi");
        } else {
            $this->transaksimodel->confirmTransaksi($chatId);
            $username = $data["chat"]["username"];
            $this->send_message($chatId, "Terimakasih untuk pembelian materinya");
            $this->sendMessageToAdmin($username . " baru saja mengkonfirmasi pembeliannya");
        }
    }

    function createTransaksi($barang, $chatId)
    {
        $cek = $this->cekTransaksi($barang, $chatId);
        if (isset($cek->barang) != true) {
            $harga = $this->cekHarga($barang);
            $this->send_message($chatId, "Sepertinya kamu belum membeli materi untuk " . $barang . ". maukah kau beli dengan harga " . $harga->harga . " ?");
            $this->send_message($chatId, "Silakan transfer ke rekening xxXXxxXXxxXX a/n. UtterAcademy");
            $this->send_message($chatId, "Jika sudah silakan ketik /konfirmasi untuk konfirmasi pembelian");
            $this->transaksimodel->createTransaksi($barang, $chatId);

        } else {
            $transaksi = $this->transaksimodel->lihatTransaksi($chatId, $barang);
            if ($transaksi[0]["status"] == 2) {
                $this->send_message($chatId, "Anda sudah membeli materi " . $barang);
            } elseif ($transaksi[0]["status"] == 1) {
                $this->send_message($chatId, "Pembelian anda belum dikonfirmasi oleh admin, tunggu ya...");
            }
        }
    }

    function cekHarga($barang)
    {
        return $this->materimodel->getHarga($barang);
    }

    function cekTransaksi($barang, $chatid)
    {
        return $this->transaksimodel->getTransaksi($barang, $chatid);
    }

    function insertUser($data)
    {
        $chatid = $data["chat"]["id"];
        $firstname = $data["chat"]["first_name"];
        $lastname = $data["chat"]["last_name"];
        $username = $data["chat"]["username"];
        $fullname = $firstname . " " . $lastname;

        if ($chatid > 0) {
            $cekUser = $this->CekUser($chatid);
            if (isset($cekUser->user_name) != true) {
                $this->usermodel->createUser([
                    ":user_id" => $chatid,
                    ":chat_id" => $chatid,
                    ":user_name" => $username,
                    ":fullname" => $fullname
                ]);
                $this->send_message($chatid, "Kalau boleh tau emailnya apa? :)");
            }
        }
    }

    function updateUser($chatId, $email)
    {
        $cekUser = $this->usermodel->getUserByEmail($email);
        if (isset($cekUser->user_name) != true) {
            $this->usermodel->updateUser(
                [":email" => $email]
                , $chatId);

            $this->send_message($chatId, "Terimakasih emailnya :)");
        }
    }

    function CekUser($chatid)
    {
        return $this->usermodel->getUserById($chatid);
    }

    function processMessageTeksComplex($chatid, $text, $messageId = null)
    {
        $this->addLog("process");
        $textSplit = explode(" ", $text);
        $this->addLog($textSplit);
        if (sizeof($textSplit) <= 1) {
            $text = "Saya Tidak Mengerti, tapi pesan saya simpan";
            $this->send_message($chatid, $text);
        } else {
            $keywordMateriGoogleDrive = array("materi", "google", "drive");
            if ($this->checkKeyword($textSplit, $keywordMateriGoogleDrive)) {
                $id_file = end($textSplit);
                $jenis = $textSplit[3];
                $judul = $this->getJudulMateri($textSplit);
                $this->materimodel->createMateriGDrive([
                    ":jenis_materi_gdrive" => $jenis,
                    ":nama_materi" => $judul,
                    ":id_file" => $id_file
                ]);
                $this->send_message($chatid, "Materi jenis : " . $jenis . "\nbernama : " . $judul . "\ndengan id : " . $id_file);
            } else {
                $text = "Saya Tidak Mengerti, tapi pesan saya simpan";
                $this->send_message($chatid, $text);
            }
        }
    }

    function checkKeyword($splitText, $keywordList)
    {
        foreach ($keywordList as $keyword) {
            $isFound = false;
            foreach ($splitText as $text) {
                if (strpos($text, $keyword) !== false) {
                    $isFound = true;
                }
            }
            if ($isFound == false) {
                return false;
            }
        }
        return true;
    }

    function getJudulMateri($splitText)
    {

        $ukuran = sizeof($splitText);
        $akhirJudul = $ukuran - 1;
        for ($i = 4; $i < $akhirJudul; $i++) {
            $arjudul[] = $splitText[$i];
        }
        return implode(" ", $arjudul);
    }

    function send_message($chatid, $text, $replyMarkup = null)
    {
        // jika text lebih dari 4096 karakter dikirim sebanyak array
        $splitMaxCharTelegram = $this->splitLongText($text);
        foreach ($splitMaxCharTelegram as $textSplit) {
            if ($replyMarkup != null) {
                $data = array(
                    'chat_id' => $chatid,
                    'text' => $textSplit,
                    'reply_markup' => $replyMarkup
                );
            } else {
                $data = array(
                    'chat_id' => $chatid,
                    'text' => $textSplit
                );
            }

            $url = $this->request_url('sendMessage');
            $result = $this->url_get_contents($url, $data);
            $this->addLog($result);
        }
        $this->logchatmodel->InsertLog([
            ":user_id" => $chatid,
            ":text" => $text
        ]);
    }

    function editMessageReplyMarkup($chatid, $messageId, $text, $replyMarkup = null)
    {
        $data = array(
            'chat_id' => $chatid,
            'text' => $text,
            'reply_markup' => $replyMarkup,
            'message_id' => $messageId

        );
        $url = $this->request_url('editMessageReplyMarkup');
        $result = $this->url_get_contents($url, $data);
        $this->addLog($result);
    }

    function deleteMessage($chatid, $messageId)
    {
        $data = array(
            'chat_id' => $chatid,
            'message_id' => $messageId
        );

        $url = $this->request_url('deleteMessage');
        $result = $this->url_get_contents($url, $data);
        $this->addLog($result);

    }

    function sendMessageFile($chatid, $text, $filePath)
    {
        // jika text lebih dari 4096 karakter dikirim sebanyak array
        $splitMaxCharTelegram = $this->splitLongText($text);
        foreach ($splitMaxCharTelegram as $textSplit) {
            if ($filePath != null) {
                $data = array(
                    'chat_id' => $chatid,
                    'text' => $filePath,
                    'document' => new CURLFile(realpath($filePath))
                );
                $url = $this->request_url('sendDocument');
                $result = $this->url_get_contentsFile($url, $data);
                $this->addLog($result);
            }
        }
    }

    function send_reply($chatid, $msgid, $text)
    {
        $data = array(
            'chat_id' => $chatid,
            'text' => $text,
            'reply_to_message_id' => $msgid
        );

        $url = $this->request_url('sendMessage');
        $result = $this->url_get_contents($url, $data);
        $this->addLog($result);
    }

    function sendMessageHTML($chatid, $text)
    {
        $data = array(
            'chat_id' => $chatid,
            'text' => $text,
            'parse_mode' => 'HTML'
        );

        $url = $this->request_url('sendMessage');
        $result = $this->url_get_contents($url, $data);
        $this->addLog($result);

    }

    function sendMessageToAdmin($message)
    {
        $rizky = "155152745";
        $chico = "677129765";
//        $this->send_message($rizky,$message);
        $this->send_message($chico, $message);
    }

    function create_response($text)
    {
        return "definisi " . $text;
    }

    function request_url($method)
    {
        return "https://api.telegram.org/bot1124577684:AAGPUFcFSJhNzCvZp99NpHlkmw6DzORUgtE/" . $method;
    }

    function serviceClient()
    {
        $client = new Google_Client();
        $client->setApplicationName('Bot utter academy');
        $client->setAuthConfig('/home/develo16/public_html/MohammadChicoHernando/API_Telegram/storage/credential/client_secret1.json');
        $client->useApplicationDefaultCredentials();
        $client->setSubject('botutter-academy@bot-utter-academy.iam.gserviceaccount.com');
        $client->setAccessType('offline');
        $client->addScope(Google_Service_Drive::DRIVE);

        return new Google_Service_Drive($client);
    }

    function shareFile($chatid, $fileId = '1A2Mj1LhCAadvXogkHY8qy7Gtmfib5j7o676')
    {
        $user_email = $this->usermodel->getUserById($chatid);
//            $email = $user_email->email;
        $email = 'hernandochico@gmail.com';

        $service = $this->serviceClient();

        $userPermission = new Google_Service_Drive_Permission(array(
            'type' => 'user',
            'role' => 'reader',
            'emailAddress' => $email
        ));

        try {
            $req = $service->permissions->create(
                $fileId, $userPermission, array('fields' => 'id')
            );
            $this->send_message($chatid, "Berhasil == " . $req->getId());
            $this->addLog($req);
            die;
        } catch (Exception $e) {
            $this->send_message($chatid, "Error = " . $e->getMessage());
        }
        die();
    }

    function processShareFile($chatid)
    {
        $transaksi = $this->transaksimodel->getConfirmedTransaksi($chatid);
        $gdrive = $this->materimodel->getMateriGDriveByJenis($transaksi->barang);

        $user_email = $this->usermodel->getUserById($chatid);
        $email = $user_email->email;

        for ($i=0;$i<sizeof($gdrive);$i++){
            $this->processPermission($chatid,$email,$gdrive[$i]["id_file"]);
        }

//        $hasil = $this->batchPermission($chatid, $email, $gdrive);

    }

    function processPermission($chatid,$email,$id)
    {
//        $transaksi = $this->transaksimodel->getConfirmedTransaksi($chatid);
//        $gdrive = $this->materimodel->getMateriGDriveByJenis($transaksi->barang);

//        $user_email = $this->usermodel->getUserById($chatid);
//        $email = $user_email->email;
//        $email = "hernandochico@gmail.com";

//        if (sizeof($gdrive) >= 1) {
//            $hasil = $this->batchPermission($chatid, $email, $gdrive);
//        } else {
//            $fileId = $gdrive[0]["id_file"];
//            $hasil = $this->permission($chatid, $email, $fileId);
//        }
            $hasil = $this->permission($chatid,$email,$id);


        if ($hasil != null) {
            $this->send_message($chatid, "Berhasil, silakan cek Email atau Google Drive ya..");
//            $this->addLog($hasil);
        }
    }

    function tesPermission($chatid){
        $email = "hernandochico@gmail.com";
//        $fileid="1HiQzXtFw8b8o8f085yEbzgV4EonK3JpOgkT2V8Cz-NY676";
            $fileid ="1_che9TFmUrKunxDYu1rgRf5OZcOdV_ZMzIC1vyaaAJo";
//            $fileid = "1JIvpe2Pi18cTdbVuC9jkkI_zpSv_uEeqA6xy7mUv3tQ";

        $hasil = $this->permission($chatid,$email,$fileid);

//            $this->send_message($chatid, "Berhasil == " . $hasil->getId());

        if ($hasil != null) {
//            $this->addLog($hasil);
            $this->send_message($chatid, "Berhasil == " . $hasil->getId());
        }
    }

    function permission($chatid, $email, $fileId)
    {

        $service = $this->serviceClient();

        $userPermission = new Google_Service_Drive_Permission(array(
            'type' => 'user',
            'role' => 'reader',
            'emailAddress' => $email
        ));
        try {
            return $service->permissions->create(
                $fileId, $userPermission, array('fields' => 'id')
            );
        } catch (Exception $e) {
            $this->send_message($chatid, "Error = " . $e->getMessage());
        }
        return null;
    }

    function batchPermission($chatid, $email, $gdriveMateri)
    {
        $service = $this->serviceClient();
        $service->getClient()->setUseBatch(true);
        $fileid = "qer";

        try {
            $batch = $service->createBatch();

            $userPermission = new Google_Service_Drive_Permission(array(
                'type' => 'user',
                'role' => 'reader',
                'emailAddress' => $email
            ));

            for ($i = 0; $i < sizeof($gdriveMateri); $i++) {
                $req = $service->permissions->create(
                    $fileid, $userPermission, array('fields' => 'id')
                );
                $batch->add($req, 'user');
            }

            $results = $batch->execute();

            foreach ($results as $result) {
                if ($result instanceof Google_Service_Exception) {
                    // Handle error
                    $this->send_message($chatid, "Error = " . $result);
                } else {
//                    $this->send_message($chatid, "Berhasil, silakan cek email yaa :) " . $result->id);
//                    $this->addLog($result);
                    return $result;
                }
            }
        } finally {
            $service->getClient()->setUseBatch(false);
        }
        return null;
    }

    function tes($chatid)
    {
        $client = new Google_Client();
        $client->setApplicationName('Bot utter academy');
        $client->setAuthConfig('/home/develo16/public_html/MohammadChicoHernando/API_Telegram/storage/credential/Botutteracademy.json');
        $client->useApplicationDefaultCredentials();
        $client->setSubject('botutter-academy@bot-utter-academy.iam.gserviceaccount.com');
        $client->setAccessType('offline');
        $client->addScope(Google_Service_Drive::DRIVE);

        $service = new Google_Service_Drive($client);
        $fileId = '1A2Mj1LhCAadvXogkHY8qy7Gtmfib5j7o';
        $fileId = '1NGSpR-QQzZRUCRBxEfQxlRg9CLKMGk9iw5zhHlO7mxQ';

        while (true) {

            try {
                $file = $service->files->get($fileId);
                $this->send_message($chatid, $file->getName());
                break;
            } catch (Exception $e) {
                $this->send_message($chatid, "Error = " . $e->getMessage());
            }
        }
    }

    function splitLongText($longString)
    {
        $output = [];
        $words = explode(chr(10), $longString);
        $maxLineLength = 4096;

        $currentLength = 0;
        $index = 0;

        foreach ($words as $word) {
            // +1 because the word will receive back the space in the end that it loses in explode()
            $wordLength = strlen($word) + 1;

            if (($currentLength + $wordLength) <= $maxLineLength) {
                $output[$index] .= $word . "\n";
                $currentLength += $wordLength;
            } else {
                $index += 1;
                $currentLength = $wordLength;
                $output[$index] = $word;
            }
        }
        return $output;
    }

    function url_get_contents($Url, $data)
    {
        if (!function_exists('curl_init')) {
            $this->addLog('CURL is not installed!');
            die('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-type: application/x-www-form-urlencoded"));
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    function url_get_contentsFile($Url, $data)
    {
        if (!function_exists('curl_init')) {
            $this->addLog('CURL is not installed!');
            die('CURL is not installed!');
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $Url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    function addLog($data)
    {
        $myfile = fopen("/home/develo16/public_html/MohammadChicoHernando/API_Telegram/storage/log/log.txt", "a") or die("Unable to open file!");
        $txt = "user id date";
        fwrite($myfile, "\n" . $data);
        fclose($myfile);
    }


}

$app->run();
<?php

//use Pos\Systems\Connection;

class UserModel
{
    public function __construct()
    {
        $this->db = new Connection();
    }

    public function getUser(){
        $query = "SELECT `user_id`,`user_name`,`fullname`,`email` FROM `user`";
        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getUserByName($name){
        $query = 'SELECT `user_id`,`user_name`,`fullname`,`email` FROM `user` WHERE `user_name` = "'.$name.'"';
        $this->db->query($query);
        return $this->db->fetch();
    }

    public function getUserByEmail($email){
        $query ='SELECT * FROM `user` WHERE `email` = "'.$email.'"';
        $this->db->query($query);
        return $this->db->fetch();
    }

    public function getUserById($id){
        $query = 'SELECT `user_id`,`user_name`,`fullname`,`email` FROM `user` WHERE `user_id` = '.$id;
//        var_dump($query);
        $this->db->query($query);
        return $this->db->fetch();
    }

    public function createUser($params){
        $query = 'INSERT INTO `user`(`user_id`,`chat_id`,`user_name`,`fullname`)
                    VALUES (:user_id, :chat_id, :user_name, :fullname)';
        $this->db->query($query,$params);
//        var_dump($query);die;
    }

    public function deleteUser($id){
        $query = "DELETE FROM `user` WHERE `user_id` = ".$id;

        $this->db->query($query);
    }

    public function updateUser($params,$id){
        $query = "UPDATE `user` SET `email` = :email WHERE `user_id` =".$id;

        $this->db->query($query,$params);
    }

}
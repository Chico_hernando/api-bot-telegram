<?php

//use Pos\Systems\Connection;

class TransaksiModel
{

    public function __construct()
    {
        $this->db = new Connection();
    }

    public function getTransaksi($barang,$id){
        $query = "SELECT * FROM `order_detail` WHERE `barang` = '".$barang."' AND `user_id` = ".$id;
        $this->db->query($query);
        return $this->db->fetch();
    }

    public function createTransaksi($barang,$id){
        $query = "INSERT INTO order_detail (barang, total, user_id) SELECT 
                        '".$barang."', produk.harga,".$id." FROM produk WHERE produk.nama_produk = '".$barang."'";
        $this->db->query($query);
    }

    public function confirmTransaksi($id){
        $query = "UPDATE `order_detail` SET `status` = 1 WHERE `user_id` = ".$id." AND `status` = 0";
        $this->db->query($query);
    }

    public function confirmAdminTransaksi($nama){
        $query = 'UPDATE `order_detail` INNER JOIN `user`
                    ON order_detail.user_id = user.user_id
                    SET order_detail.status = 2 
                    WHERE user.user_name = "'.$nama.'" AND order_detail.status = 1';
        $this->db->query($query);
    }

    public function getUnconfirmedTransaksi(){
        $query = "SELECT user.user_name AS `username` ,
                    user.fullname AS `fullname`, 
                    order_detail.barang AS `barang`,
                    order_detail.total AS `total`,
                    user.user_id AS `user_id`
                    from `user`
                    INNER JOIN `order_detail` ON user.user_id = order_detail.user_id
                    WHERE order_detail.status = 1";
        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getConfirmedTransaksi($id){
        $query = "SELECT * FROM `order_detail` WHERE `user_id` = ".$id." AND `status` = 2 ORDER BY `order_id` DESC LIMIT 1";
        $this->db->query($query);
        return $this->db->fetch();
    }

    public function lihatTransaksi($id, $barang = null){
        $query = "SELECT * FROM `order_detail` WHERE `user_id` = ".$id.($barang !== null?" AND `barang` = '".$barang."'":null);
        $this->db->query($query);
        return $this->db->fetchAlla();
    }

}
<?php


class LogChatModel
{
    public function __construct()
    {
        $this->db = new Connection();
    }

    public function InsertLog($params){
        $query = "INSERT INTO `log_chat_bot` (`user_id`, `text`)
                    VALUES (:user_id,:text)";
        $this->db->query($query,$params);
    }

    public function getLog($id){
        $query = "SELECT `text` FROM `log_chat_bot` WHERE `user_id` =".$id." ORDER BY `id_log` DESC LIMIT 1";
        $this->db->query($query);
        return $this->db->fetch();
    }

    public function get2Log($id){
        $query = "SELECT `text` FROM `log_chat_bot` WHERE `user_id` =".$id." ORDER BY `id_log` DESC LIMIT 1, 2";
        $this->db->query($query);
        return $this->db->fetch();
    }
}
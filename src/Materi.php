<?php

require '../vendor/autoload.php';
include ("MateriModel.php");
include ("../Request.php");
include ("../Response.php");

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App();
class Materi
{
    private $request;

    private $response;
    /**
     * @var MateriModel
     */
    private $materimodel;

    public function __construct()
    {
        $this->materimodel = new MateriModel();
        $this->request = new \Request();
        $this->response = new \Response();
    }

}
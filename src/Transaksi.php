<?php
require '../vendor/autoload.php';
include ("TransaksiModel.php");
include ("../Request.php");
include ("../Response.php");

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App();


class Transaksi
{
    /**
     * @var TransaksiModel
     */
    private $transaksimodel;
    /**
     * @var \Request
     */
    private $request;
    /**
     * @var \Response
     */
    private $response;

    public function __construct()
    {
        $this->transaksimodel = new TransaksiModel();
        $this->request = new \Request();
        $this->response = new \Response();
    }



}
<?php
require '../vendor/autoload.php';
include ("ShopsModel.php");
include ("../Request.php");
include ("../Response.php");

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App();

$app->post("/add" , Shops::class . ":add");
$app->post("/create" , Shops::class . ":create");
$app->post("/update" , Shops::class. ":update");
$app->post("/deleteItem" , Shops::class . ":deleteItem");
$app->post("/deleteCart", Shops::class . ":deleteCart");
$app->get("/{user_id}" , Shops::class . ":detail");



class Shops implements Constants
{

    private $shopsmodel;

    private $request;

    private $response;

    public function __construct()
 {
     $this->shopsmodel = new ShopsModel();
     $this->request = new \Request();
     $this->response = new \Response();
 }

 public function create(Request $request, Response $response){
     $parse = $request->getParsedBody();
     $cekCart = $this->shopsmodel->cekCart($parse["user_id"]);
     if (isset($cekCart->cart_id) == true){
         echo "Keranjang sudah ada";
     }else{
         $result = $this->_createCart($parse);

//    echo "Berhasil membuat keranjang";
    return $this->response->publish($result,"Success Create Cart",self::SUCCESS);
     }

 }

 public function add(Request $request,Response $response ){
        $parse = $request->getParsedBody();
     $cekCart = $this->shopsmodel->cekCart($parse["user_id"]);
     if (isset($cekCart->cart_id) != true){
         return $this->response->publish ( null, "Cart Not Found", self::NOT_FOUND );
     }else{
         $result = $this->_addCart($parse);
         return $this->response->publish($result,"Success Add Cart",self::SUCCESS);
     }
 }

 public function detail(Request $request, Response $response){
     $user_id = $request->getAttribute('user_id');
     $cekCart = (array) $this->shopsmodel->cekCart($user_id);
     if ( isset ( $cekCart->scalar )){
         return $this->response->publish ( null, "Cart Not Found", self::NOT_FOUND);
     } else{
            $cekCart = $this->getShopsCartItem($cekCart,$user_id);
         return $this->response->publish($cekCart,"Success get Cart Detail",self::SUCCESS);
     }
 }

 public function update(Request $request, Response $response){
     $parse = $request->getParsedBody();
     $cekTotal = $this->shopsmodel->getTotal($parse["user_id"]);
//     var_dump($cekTotal);
     echo $cekTotal->jumlah;
 }

 public function deleteItem(Request $request, Response $response){
     $parse = $request->getParsedBody();
     $cekCart = $this->shopsmodel->cekCart($parse["user_id"]);
     if (isset($cekCart->cart_id) != true){
         return $this->response->publish ( null, "Cart Not Found", self::NOT_FOUND );
     }else{
         $this->shopsmodel->deleteItem($parse["id_produk"],$parse["user_id"]);
         return $this->response->publish ( null, "Success Delete From Cart", self::SUCCESS );
     }
 }

 public function deleteCart(Request $request, Response $response){
     $parse = $request->getParsedBody();
     $cekCart = $this->shopsmodel->cekCart($parse["user_id"]);
     if (isset($cekCart->cart_id) != true){
         return $this->response->publish ( null, "Cart Not Found", self::NOT_FOUND );
     }else{
         $this->shopsmodel->deleteCart($parse["user_id"]);
         return $this->response->publish ( null, "Success Delete Cart", self::SUCCESS );
     }
 }

 private function _createCart($parse){
     $this->shopsmodel->createCart([
         ":user_id"=>$parse["user_id"],
         ":status"=>$parse["status"]
     ]);
     return [
         "user_id"=>$parse["user_id"],
         "status"=>$parse["status"]
     ];
 }

 private function _addCart($parse){
     $this->shopsmodel->addCart(
         $parse["id_produk"],
         $parse["user_id"]
     );
     return [
       "id_produk"=>$parse["id_produk"],
       "user_id"=>$parse["user_id"]
     ];
 }

 private function getShopsCartItem($cekCart,$user_id){
        $cekCart['item'] = (array) $this->shopsmodel->getCartItem($user_id);

        return $cekCart;
 }

}
header('Content-type: application/json; charset=utf-8');
$app->run();
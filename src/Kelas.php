<?php

require '../vendor/autoload.php';
include ("KelasModel.php");
include ("../Request.php");
include ("../Response.php");

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App();
class Kelas
{
    private $request;

    private $response;
    /**
     * @var KelasModel
     */
    private $kelasmodel;

    public function __construct()
    {
        $this->kelasmodel = new KelasModel();
        $this->request = new \Request();
        $this->response = new \Response();
    }
}
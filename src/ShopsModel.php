<?php

//use Pos\Systems\Connection;

class ShopsModel
{
    public function __construct()
    {
        $this->db = new Connection();
    }

    public function cekCart($user_id){
        $query = "SELECT * FROM cart WHERE user_id = ".$user_id;

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function createCart($params){
        $query = "INSERT INTO `cart`(`user_id`,`status`) VALUES 
                    (:user_id,:status)";
        $this->db->query($query,$params);

    }

    public function addCart($id_produk,$id_user){
        $query = "INSERT INTO cart_detail ( cart_id, id_produk) SELECT 
                    cart.cart_id,".$id_produk." FROM cart WHERE cart.user_id = ".$id_user;

        $this->db->query($query);

        $this->updateCart($id_user);
    }

    public function updateCart($id_user){
        $total = $this->getTotal($id_user);
        $query = "UPDATE `cart` SET `total` = ".$total->jumlah." WHERE user_id = ".$id_user;

        $this->db->query($query);
    }

    public function getTotal($id_user){
        $query = "SELECT SUM(produk.harga) as `jumlah` FROM `cart` 
            INNER JOIN `cart_detail` ON cart.cart_id = cart_detail.cart_id 
            INNER JOIN `produk` ON cart_detail.id_produk = produk.id_produk
            WHERE cart.user_id = ".$id_user;

        $this->db->query($query);

        return $this->db->fetch();
    }

    public function deleteItem($id_produk,$id_user){
        $query = "DELETE d FROM cart_detail d 
            INNER JOIN cart as b ON b.cart_id = d.cart_id
            WHERE b.user_id = ".$id_user." AND d.id_produk = ".$id_produk;

        $this->db->query($query);

        $this->updateCart($id_user);
    }

    public function deleteCart($id_user){
        $this->deleteCartItem($id_user);

        $query = "DELETE FROM `cart` WHERE user_id = ".$id_user;

        $this->db->query($query);

    }

    public function deleteCartItem($id_user){
        $query = "DELETE d FROM cart_detail d 
            INNER JOIN cart as b ON b.cart_id = d.cart_id
            WHERE b.user_id = ".$id_user;

        $this->db->query($query);
    }

    public function getCartItem($user_id){
        $query = "SELECT 
        produk.nama_produk,
        produk.harga
        FROM `cart`
        INNER JOIN `cart_detail` ON cart.cart_id = cart_detail.cart_id
        INNER JOIN `produk` ON cart_detail.id_produk = produk.id_produk
        WHERE cart.user_id = ".$user_id;

        $this->db->query($query);

        return $this->db->fetchAll();
    }

}
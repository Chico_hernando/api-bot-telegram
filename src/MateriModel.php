<?php
//use Pos\Systems\Connection;

class MateriModel
{
    public function __construct()
    {
        $this->db = new Connection();
    }

    public function getHarga($barang){
        $query = "SELECT `harga` FROM produk WHERE `nama_produk` = '".$barang."'";
        $this->db->query($query);
        return $this->db->fetch();
    }

    public function createJudul($params){
        $query = "INSERT INTO `materi` (`user_id`, `jenis_materi`, `judul_materi`)
                    VALUES (:user_id, :jenis_materi, :judul_materi)";
        $this->db->query($query,$params);
    }

    public function createMateriGDrive($params){
        $query = 'INSERT INTO `materi_google_drive` (`jenis_materi_gdrive`, `nama_materi`, `id_file`)
                    VALUES (:jenis_materi_gdrive, :nama_materi, :id_file)';
        $this->db->query($query,$params);
    }

    public function getMateriGDrive(){
        $query = "SELECT `jenis_materi_gdrive`,`nama_materi`,`id_file` FROM `materi_google_drive`";
        $this->db->query($query);
        return $this->db->fetchAlla();
    }

    public function getMateriGDriveByJenis($jenis){
        $query = "SELECT `nama_materi`,`id_file` FROM `materi_google_drive` WHERE `jenis_materi_gdrive` = '".$jenis."'";
        $this->db->query($query);
        return $this->db->fetchAlla();
    }
}
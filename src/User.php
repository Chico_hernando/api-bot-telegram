<?php
//header("Content-Type: application/json; charset=UTF-8");
require '../vendor/autoload.php';
include ("UserModel.php");
include ("../Request.php");
include ("../Response.php");

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app = new \Slim\App();

$app->get("/getUserAll", User::class . ":getUserAll");
$app->get("/getUser" , User::class . ":getUser");
$app->post("/InsertUser" , User::class . ":InsertUser");
$app->post("/UpdateUser", User::class . ":UpdateUser");
$app->get("/DeleteUser" , User::class . ":DeleteUser");


class User implements Constants
{

    private $usermodel;
    /**
     * @var \Request
     */
    private $request;
    /**
     * @var \Response
     */
    private $response;

    public function __construct()
    {

        $this->usermodel = new UserModel();
        $this->request = new \Request();
        $this->response = new \Response();

    }

    public function getUserAll(Request $request, Response $response){

//        global $usermodel;
        $user = $this->usermodel->getUser();

        return $this->response->publish($user,"Success Get All User",self::SUCCESS);
    }

    public function getUser( Request $request, Response $response){
        $name = $_GET['name'];
//        $name = $request->getAttribute('name');
        $result = $this->usermodel->getUserByName($name);
        if (isset($result->user_name) != true){
//            var_dump("nama tidak ada");
            return $this->response->publish(null,"Failed Get User",self::NOT_FOUND);
        }else
//        print_r($result);
        return $this->response->publish($result,"Success Get User", self::SUCCESS);
    }

    public function InsertUser(Request $request, Response $response){
        $user = $request->getParsedBody();
        $result = $this->_InsertUser($user);

//        print_r($result);
        return $this->response->publish($result,"Success Add User",self::SUCCESS);

    }

    public function DeleteUser(Request $request, Response $response){
        $user_id = $_GET["user_id"];
        $user = $this->usermodel->getUserById($user_id);
        if (isset($user->user_name) != true){
            echo "User tidak ada";
        }else{
        $this->usermodel->deleteUser($user_id);
            return $this->response->publish(null, "Success Delete User", self::SUCCESS);
        }
    }

    public function UpdateUser(Request $request, Response $response){
        $user = $request->getParsedBody();
        $cekUser = $this->usermodel->getUserById($user["user_id"]);
        if (isset($cekUser->user_name) != true){
            echo "User tidak ada";
        }else{
            $result = $this->_UpdateUser($user);
            return $this->response->publish($result, "Success Update User", self::SUCCESS);
        }

    }

    private function _UpdateUser($user){
        $this->usermodel->updateUser([
            ":email"=>$user["email"]
        ],$user["user_id"]);
        return [
            "user_id"=>$user["user_id"],
            "email"=>$user["email"]
        ];
    }

    private function _InsertUser($user){
        $this->usermodel->createUser([
            ":user_id"=>$user["user_id"],
            ":chat_id"=>$user["chat_id"],
            ":user_name"=>$user["username"],
            ":fullname"=>$user["fullname"]
        ]);
        return [
            "user_id"=>$user["user_id"],
            "chat_id"=>$user["chat_id"],
            "user_name"=>$user["username"],
            "fullname"=>$user["fullname"]
        ];

    }
}
$app->run();
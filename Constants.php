<?php

interface Constants
{
    /**
     * Status code
     */
    const SUCCESS = 200;
    const FORBIDEN = 403;
    const NOT_FOUND = 404;
    const ERROR = 500;

    /**
     * Action Configuration
     */
    const ACTION_CREATE = 0;
    const ACTION_READ = 1;
    const ACTION_UPDATE = 2;
    const ACTION_DELETE = 3;

    /**
     * Status Configuration
     */
    const CART_STATUS = 0;
    const BUY_STATUS = 1;
}